import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NietoSComponent } from './nieto-s.component';

describe('NietoSComponent', () => {
  let component: NietoSComponent;
  let fixture: ComponentFixture<NietoSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NietoSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NietoSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
