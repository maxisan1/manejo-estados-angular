import { Component } from '@angular/core';
import { ContadorService } from '../contador.service';

@Component({
  selector: 'app-nieto-s',
  templateUrl: './nieto-s.component.html',
  styleUrls: ['./nieto-s.component.css']
})
export class NietoSComponent {

  constructor(public contadorService: ContadorService) { }

}
