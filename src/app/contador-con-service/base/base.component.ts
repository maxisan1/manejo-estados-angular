import { Component, OnInit } from '@angular/core';
import { ContadorService } from '../contador.service';

@Component({
  selector: 'app-base-s',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseSComponent implements OnInit {

  constructor(public contadorService: ContadorService) { }

  ngOnInit(): void {
  }

}
