import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContadorService {
  contador = 0
  constructor() { }

  incrementar() {
    this.contador++
  }

  decrementar() {
    this.contador--
  }

  multiplicar() {
    this.contador *= 2
  }

  dividir() {
    this.contador /=2
  }

  reset() {
    this.contador = 0
  }
}
