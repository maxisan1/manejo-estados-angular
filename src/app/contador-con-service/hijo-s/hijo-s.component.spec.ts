import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HijoSComponent } from './hijo-s.component';

describe('HijoSComponent', () => {
  let component: HijoSComponent;
  let fixture: ComponentFixture<HijoSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HijoSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HijoSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
