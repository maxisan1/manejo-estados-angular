import { Component } from '@angular/core';
import { ContadorService } from '../contador.service';

@Component({
  selector: 'app-hijo-s',
  templateUrl: './hijo-s.component.html',
  styleUrls: ['./hijo-s.component.css']
})
export class HijoSComponent {

  constructor(public contadorService: ContadorService) { }

}
