import { Component } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { DECREMENTAR, INCREMENTAR } from '../redux/constants';
import { DecrementarAction, IncrementarAction } from '../redux/contador.actions';
import { AppState } from '../redux/store';

@Component({
  selector: 'app-base-ngrx',
  templateUrl: './base-ngrx.component.html',
  styleUrls: ['./base-ngrx.component.css']
})

export class BaseNgrxComponent {
  contador: number = 0;

  constructor(private store: Store<AppState>) {
    this.store.subscribe(
      state => this.contador = state.contador
    )
  }
  

  incrementar() {
    const accion = new IncrementarAction 
    this.store.dispatch(accion)
  }

  decrementar() {
    this.store.dispatch(new DecrementarAction)
  }
}
