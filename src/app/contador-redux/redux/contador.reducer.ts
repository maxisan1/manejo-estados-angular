import { Action } from "@ngrx/store";
import {
    INCREMENTAR,
    DECREMENTAR,
    MULTIPLICAR,
    DIVIDIR,
    RESET
} from './constants'

export function contadorReducer(state: number = 0, action: Action) {

    switch(action.type) {
        case INCREMENTAR:
            return state + 1

        case DECREMENTAR:
            return state - 1
        
        case MULTIPLICAR:
            return state * 2
        
        case DIVIDIR:
            return state / 2
        
        case RESET:
            return 0
            
        default:
            return state;
    }
}