import { Action } from '@ngrx/store'
import * as c from './constants'

export class IncrementarAction implements Action {
    readonly type = c.INCREMENTAR
}

export class DecrementarAction implements Action {
    readonly type = c.DECREMENTAR
}

export class MultiplicarAction implements Action {
    readonly type = c.MULTIPLICAR
}

export class DividirAction implements Action {
    readonly type = c.DIVIDIR
}

export class ResetAction implements Action {
    readonly type = c.RESET
}

