import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { DIVIDIR, MULTIPLICAR } from '../redux/constants';
import { DividirAction, MultiplicarAction } from '../redux/contador.actions';
import { AppState } from '../redux/store';

@Component({
  selector: 'app-hijo-ngrx',
  templateUrl: './hijo-ngrx.component.html',
  styleUrls: ['./hijo-ngrx.component.css']
})
export class HijoNgrxComponent {

  contador = 0

  constructor(private store: Store<AppState> ) {
    this.store.subscribe(
      state => this.contador = state.contador
    )
  }

  multiplicar() {
    this.store.dispatch(new MultiplicarAction)
  }

  
  dividir() {
    this.store.dispatch(new DividirAction)
  }


}
