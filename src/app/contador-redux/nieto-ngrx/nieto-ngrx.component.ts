import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { RESET } from '../redux/constants';
import { ResetAction } from '../redux/contador.actions';
import { AppState } from '../redux/store';

@Component({
  selector: 'app-nieto-ngrx',
  templateUrl: './nieto-ngrx.component.html',
  styleUrls: ['./nieto-ngrx.component.css']
})
export class NietoNgrxComponent {

  contador = 0

  constructor(private store: Store<AppState>) {
    this.store.subscribe(
      state => this.contador = state.contador
    )
  }

  reset() {
    this.store.dispatch(new ResetAction)
  }
}
