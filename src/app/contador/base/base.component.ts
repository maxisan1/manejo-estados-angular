import { Component } from '@angular/core';

@Component({
  selector: 'app-base-io',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent {
  contador:number = 0

  constructor() {
  }

  title = 'redux-app';

  incrementar() {
    this.contador++
  }

  decrementar() {
    this.contador--
  }
}
