import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';

import { HijoComponent } from './contador/hijo/hijo.component';
import { NietoComponent } from './contador/nieto/nieto.component';
import { BaseComponent } from './contador/base/base.component';

import { BaseSComponent } from './contador-con-service/base/base.component';
import { HijoSComponent } from './contador-con-service/hijo-s/hijo-s.component';
import { NietoSComponent } from './contador-con-service/nieto-s/nieto-s.component';

import { contadorReducer } from './contador-redux/redux/contador.reducer';
import { BaseNgrxComponent } from './contador-redux/base-ngrx/base-ngrx.component';
import { HijoNgrxComponent } from './contador-redux/hijo-ngrx/hijo-ngrx.component';
import { NietoNgrxComponent } from './contador-redux/nieto-ngrx/nieto-ngrx.component';

@NgModule({
  declarations: [
    AppComponent,
    HijoComponent,
    NietoComponent,
    BaseComponent,
    BaseSComponent,
    HijoSComponent,
    NietoSComponent,
    BaseNgrxComponent,
    HijoNgrxComponent,
    NietoNgrxComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({ contador: contadorReducer })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
